# FastJet support for user codes in CMake

Downstream CMake builds may use the [configuration files][1] provided in this directory.
They can be used with the following syntax in your `CMakeLists.txt`:
```cmake
  find_package(FastJet CONFIG REQUIRED)
  find_package(FastJetTools CONFIG REQUIRED)
```
If you are sure no other FindFastJet.cmake modules will be found, you may also
omit the `CONFIG` option.

[1]: https://cmake.org/cmake/help/latest/manual/cmake-developer.7.html#package-configuration-file
