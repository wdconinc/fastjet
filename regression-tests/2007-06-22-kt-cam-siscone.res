
FASTJET regression tests on Fri Jun 22 15:51:19 CEST 2007

Reference exec: /ada1/lpthe/salam/tmp/fastjet-2.1.0/example/fastjet_timing_plugins
Test exec:      ../example/fastjet_timing_plugins
Data file:      /ada1/lpthe/salam/work/fastjet/data/Pythia-PtMin50-LHC-1000ev.dat
Nev used:       1000
Alg list:       kt, cam, siscone
Filter:         grep -v -e '^#' -e 'strategy' -e '^Algorithm:'


******* Running reference result for kt ()
Running strategy -04 for kt
Number of differences wrt ref = 0
Running strategy -03 for kt
Number of differences wrt ref = 0
Running strategy -02 for kt
Number of differences wrt ref = 0
Running strategy -01 for kt
Number of differences wrt ref = 0
Running strategy +02 for kt
Number of differences wrt ref = 0
Running strategy +03 for kt
Number of differences wrt ref = 0
Running strategy +04 for kt
Number of differences wrt ref = 0
Running strategy +00 for kt
Number of differences wrt ref = 0


******* Running reference result for cam (-cam)
Running strategy -04 for cam
Number of differences wrt ref = 0
Running strategy -03 for cam
Number of differences wrt ref = 0
Running strategy -02 for cam
Number of differences wrt ref = 0
Running strategy -01 for cam
Number of differences wrt ref = 0
Running strategy +02 for cam
Number of differences wrt ref = 0
Running strategy +03 for cam
Number of differences wrt ref = 0
Running strategy +04 for cam
Number of differences wrt ref = 0
Running strategy +00 for cam
Number of differences wrt ref = 0
Running strategy +12 for cam
Number of differences wrt ref = 0
Running strategy +13 for cam
Number of differences wrt ref = 0
Running strategy +14 for cam
Number of differences wrt ref = 0


******* Running reference result for siscone (-siscone -npass 0)
Running strategy 00 for siscone
Number of differences wrt ref = 0
